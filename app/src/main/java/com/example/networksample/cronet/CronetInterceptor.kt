package com.example.networksample.cronet

import android.util.Log
import okhttp3.Cookie
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.internal.connection.RealCall
import okhttp3.internal.http.receiveHeaders
import okio.Buffer
import org.chromium.net.CronetException
import org.chromium.net.UploadDataProviders
import org.chromium.net.UrlRequest
import java.io.IOException
import java.net.HttpURLConnection
import java.nio.charset.Charset
import java.util.concurrent.CountDownLatch

/**
 * TODO 类描述
 * created by carter on 2021/1/26 20:40
 */
class CronetInterceptor : Interceptor {

    private var isCronetOpen = true

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val call = chain.call() as RealCall
        if (!isCronetOpen) {
            return chain.proceed(request)
        } else {
            val url = request.url.toString()
            val method = request.method
            val requestBody = request.body
            var body: String? = null
            if (requestBody != null) {
                val buffer = Buffer()
                requestBody.writeTo(buffer)
                var charset = Charset.forName("utf-8")
                val contentType = requestBody.contentType()
                if (contentType != null) {
                    charset = contentType.charset(charset)
                }
                body = buffer.readString(charset!!)
            }
            val results = arrayOf<Response?>(null)
            val exceptions = arrayOf<Exception?>(null)
            val countDownLatch = CountDownLatch(1)
            val callback: UrlRequest.Callback = object : CronetRequestCallBack(call, request) {
                override fun onSuccess(result: Response) {
                    countDownLatch.countDown()
                    results[0] = result
                }

                override fun onFailed(error: CronetException) {
                    countDownLatch.countDown()
                    exceptions[0] = error
                }
            }
            val builder = Cronet.getInstance().newCronetRequestBuilder(url, callback)
            builder.setHttpMethod(method)
            //设置request header
            val headers = request.headers
            val iterator = headers.iterator()
            while (iterator.hasNext()) {
                val pair = iterator.next()
                val key = pair.component1()
                val value = pair.component2()
                builder.addHeader(key, value)
            }
            //设置request cookie
            val cookieJar = call.client.cookieJar
            val cookies = cookieJar.loadForRequest(request.url)
            if (!cookies.isEmpty()) {
                builder.addHeader("Cookie", cookieHeader(cookies))
            }
            //设置request body
            if (body != null) {
                builder.setUploadDataProvider(
                    UploadDataProviders.create(body.toByteArray()),
                    Cronet.getInstance().executor
                )
            }
            builder.build().start()
            try {
                countDownLatch.await()
            } catch (e: InterruptedException) {
                e.printStackTrace()
                exceptions[0] = e
            }

            // cronet请求成功
            if (results[0] != null) {
                val response = results[0]

                //处理response cookie
                cookieJar.receiveHeaders(request.url, response!!.headers)

                return results[0]!!
            }
            // cronet请求失败
            // TODO 降级策略
            if (exceptions[0] != null) {
                exceptions[0]!!.printStackTrace()
                throw exceptions[0]!!
            }

            return chain.proceed(request)
        }
    }

    private fun cookieHeader(cookies: List<Cookie>): String = buildString {
        cookies.forEachIndexed { index, cookie ->
            if (index > 0) append("; ")
            append(cookie.name).append('=').append(cookie.value)
        }
    }
}