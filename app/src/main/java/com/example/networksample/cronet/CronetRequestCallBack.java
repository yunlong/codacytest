package com.example.networksample.cronet;

import android.text.TextUtils;
import android.util.Log;

import org.chromium.net.CronetException;
import org.chromium.net.UrlRequest;
import org.chromium.net.UrlResponseInfo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.List;
import java.util.Map;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.connection.RealCall;

/**
 * TODO 类描述
 * created by carter on 2021/2/4 16:54
 */
public abstract class CronetRequestCallBack extends UrlRequest.Callback {
    private final static String TAG = "cronetcallback";
    protected ByteArrayOutputStream mBytesReceived = new ByteArrayOutputStream();
    protected WritableByteChannel mReceiveChannel = Channels.newChannel(mBytesReceived);
    protected Request okRequest;
    protected RealCall call;

    public CronetRequestCallBack(RealCall call, Request okRequest) {
        this.okRequest = okRequest;
        this.call = call;
    }

    @Override
    public void onRedirectReceived(UrlRequest request, UrlResponseInfo info, String newLocationUrl) {
        try {
            if (call.getClient().followRedirects()) {
                URL newUrl = new URL(newLocationUrl);
                //对于http请求，协议只有http和https
                boolean sameProtocol = newUrl.getProtocol().equals(okRequest.url().url().getProtocol());
                if (call.getClient().followSslRedirects() && sameProtocol) {
                    request.followRedirect();
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponseStarted(UrlRequest request, UrlResponseInfo info) {
        request.read(ByteBuffer.allocateDirect(32 * 1024));
    }

    @Override
    public void onReadCompleted(UrlRequest request, UrlResponseInfo info, ByteBuffer byteBuffer) {
        byteBuffer.flip();
        try {
            mReceiveChannel.write(byteBuffer);
        } catch (IOException e) {
            Log.i(TAG, "IOException during ByteBuffer read. Details: ", e);
        }
        byteBuffer.clear();
        request.read(byteBuffer);
    }

    @Override
    public void onSucceeded(UrlRequest request, UrlResponseInfo info) {
        Response.Builder builder = new Response.Builder();
        //设置状态码
        builder.code(info.getHttpStatusCode());
        //设置header
        String contentTypeStr = null;
        String protocolStr = null;
        Map<String, List<String>> allHeaders = info.getAllHeaders();
        Set<String> keySet = allHeaders.keySet();
        for (String key : keySet) {
            if ("Content-Type".equals(key)) {
                List<String> ContentTypeValues = allHeaders.get(key);
                if (ContentTypeValues != null && !ContentTypeValues.isEmpty()) {
                    contentTypeStr = ContentTypeValues.get(0);
                }
            }
            if ("Protocol".equals(key)) {
                List<String> ProtocolValues = allHeaders.get(key);
                if (ProtocolValues != null && !ProtocolValues.isEmpty()) {
                    protocolStr = ProtocolValues.get(0);
                }
            }
            List<String> values = allHeaders.get(key);
            StringBuilder headerSB = new StringBuilder();
            for (String value : values) {
                headerSB.append(value + ";");
            }
            String header = "";
            if (headerSB.length() > 0) {
                header = headerSB.substring(0, headerSB.length() - 1);
            }
            builder.addHeader(key, header);
        }
        //设置body
        if (!TextUtils.isEmpty(contentTypeStr)) {
            MediaType mediaType = MediaType.parse(contentTypeStr);
            ResponseBody responseBody = ResponseBody.create(mBytesReceived.toByteArray(), mediaType);
            builder.body(responseBody);
        }
        //设置request
        builder.request(this.okRequest);
        //设置protocol
        //TODO 如何判断protocol
        builder.protocol(Protocol.HTTP_1_1);
        //设置message
        builder.message(info.getHttpStatusText());
        onSuccess(builder.build());
    }

    @Override
    public void onFailed(UrlRequest request, UrlResponseInfo info, CronetException error) {
        onFailed(error);
    }

    public abstract void onSuccess(Response response);

    public abstract void onFailed(CronetException error);
}
