package com.example.networksample.cronet;

import org.chromium.net.CronetEngine;
import org.chromium.net.UrlRequest;

import java.util.concurrent.Executor;

import okhttp3.OkHttpClient;

/**
 * TODO 类描述
 * created by carter on 2021/2/24 14:32
 */
public class Cronet {

    private static Cronet instance = new Cronet();

    private Cronet() {

    }

    public static Cronet getInstance() {
        return instance;
    }

    public CronetEngine engine;
    public Executor executor;

    public static void init(CronetEngine engine, Executor executor) {
        instance.engine = engine;
        instance.executor = executor;
    }

    public UrlRequest.Builder newCronetRequestBuilder(String url, UrlRequest.Callback callback) {
        return engine.newUrlRequestBuilder(url, callback, executor);
    }
}
