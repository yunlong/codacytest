package com.example.networksample;

import android.text.TextUtils;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * TODO 类描述
 * created by carter on 2021/2/4 20:54
 */
class SpecifiedStringInterceptor implements Interceptor {

    private String getSpecifiedString() {
        return "查询成功";
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        // 只对小数据进行处理
        try {
            int maxContentLength = 1024 * 10;
            if (response.body().contentLength() < maxContentLength) {
                byte[] bytes = response.body().bytes();
                byte[] copyBytes = new byte[bytes.length];
                System.arraycopy(bytes, 0, copyBytes, 0, bytes.length);
                String responseBodyStr = new String(bytes);
                String specifiedString = getSpecifiedString();
                if (!TextUtils.isEmpty(responseBodyStr) && !TextUtils.isEmpty(specifiedString) && responseBodyStr
                        .contains(specifiedString)) {
                    Log.i("xyl", "url:" + request.url().url().toString());
                }
                ResponseBody responseBody = ResponseBody.create(copyBytes, response.body().contentType());
                Response newResponse = response.newBuilder().body(responseBody).build();
                return newResponse;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

}
