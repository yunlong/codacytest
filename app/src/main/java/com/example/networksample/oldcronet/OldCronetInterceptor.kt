package com.example.networksample.oldcronet

import com.example.networksample.cronet.Cronet
import okhttp3.Cookie
import okhttp3.Interceptor
import okhttp3.Protocol
import okhttp3.Response
import okhttp3.internal.http.RealResponseBody
import okhttp3.internal.http.receiveHeaders
import okio.buffer
import okio.sink
import okio.source
import java.io.IOException
import java.net.HttpURLConnection

/**
 * 用cronet代替系统发送请求的拦截器
 * created by carter on 2021/2/24 14:26
 */

//todo 处理cronet的各种异常
class OldCronetInterceptor : Interceptor {

    private var isCronetOpen = true

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        if (!isCronetOpen) {
            return chain.proceed(request)
        } else {
            try {
                val url = request.url.toUrl()
                val connection =
                    Cronet.getInstance().engine.openConnection(url) as HttpURLConnection
                //设置request header
                request.headers.names().forEach {
                    connection.addRequestProperty(it, request.headers[it])
                }
                //设置request cookie
//                val cookieJar = Cronet.getInstance().client.cookieJar
//                val cookies = cookieJar.loadForRequest(request.url)
//                if (cookies.isNotEmpty()) {
//                    connection.addRequestProperty("Cookie", cookieHeader(cookies))
//                }

                connection.requestMethod = request.method
                request.body?.let { requestBody ->
                    requestBody.contentType()?.let { contentType ->
                        connection.setRequestProperty("Content-Type", contentType.toString())
                    }

                    connection.doOutput = true
                    val outputStream = connection.outputStream
                    val sink = outputStream.sink().buffer()
                    requestBody.writeTo(sink)
                    sink.flush()
                    outputStream.close()
                }


                val responseCode = connection.responseCode

                //todo 处理重定向
                if (responseCode in 300..310) {

                }


                val responseBuilder = Response.Builder()
                responseBuilder
                    .request(request)
                    //todo 区分具体协议
                    .protocol(Protocol.HTTP_1_1)
                    .code(responseCode)
                    .message(connection.responseMessage ?: "")

                val responseHeaders = connection.headerFields
                responseHeaders.entries.forEach { entry ->
                    entry.value.forEach { value ->
                        responseBuilder.addHeader(entry.key, value)
                    }
                }

                val bodySource =
                    (if (responseCode in 200..399) connection.inputStream else connection.errorStream).source()
                        .buffer()
                responseBuilder.body(
                    RealResponseBody(
                        responseHeaders["Content-Type"]?.last(),
                        responseHeaders["Content-Length"]?.last()?.toLong() ?: 0,
                        bodySource
                    )
                )

                val response = responseBuilder.build()

                //处理response cookie
//                cookieJar.receiveHeaders(request.url, response.headers)

                return response
            } catch (e: IOException) {
                e.printStackTrace()
                // cronet发生请求错误，开始进行积分策略

            } catch (t: Throwable) {
                t.printStackTrace()
                // 发生其他异常，可能不是cronet导致的

            } finally {
                return chain.proceed(request)
            }
        }
    }

    private fun cookieHeader(cookies: List<Cookie>): String = buildString {
        cookies.forEachIndexed { index, cookie ->
            if (index > 0) append("; ")
            append(cookie.name).append('=').append(cookie.value)
        }
    }
}