package com.example.networksample;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.networksample.cronet.Cronet;
import com.example.networksample.cronet.CronetInterceptor;
import com.example.networksample.cronet.CronetRequestCallBack;

import org.chromium.net.CronetEngine;
import org.chromium.net.CronetException;
import org.chromium.net.NetworkException;
import org.chromium.net.RequestFinishedInfo;
import org.chromium.net.UploadDataProviders;
import org.chromium.net.UrlRequest;
import org.chromium.net.UrlResponseInfo;
import org.chromium.net.impl.CronetUrlRequestContext;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Proxy;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Dns;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * TODO 类描述
 * created by carter on 2021/1/14 15:59
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "xyl";

    private OkHttpClient okHttpClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initOkhttp();

        initCronet();

        List<String> list = new ArrayList<>();
        list.add("123");
        list.add("34");
        list.add("342");
        Log.i(TAG, list.toString());
    }

    public void okhttp(View view) {
        okhttpRequest();
    }

    public void cronet(View view) {
        cronetRequest();
    }

    private void okhttpRequest() {
        for (int i = 0; i < 1; i++) {
            Request request = new Request.Builder()
//                .url("https://203.107.54.210/onebox/basketball/nba?key=189f1351be5be9d398daab72f3f60ab2")
//                .url("https://op.juhe.cn/onebox/basketball/nba?key=189f1351be5be9d398daab72f3f60ab2")
                    .url("https://www.baidu.com")
                    .get()
                    .build();
            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {

                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    ResponseBody body = response.body();
                    String str = body.string();
                    Log.i(TAG, str);
                }
            });
        }
    }

    private void cronetRequest() {
//        Observable.create(new ObservableOnSubscribe<Object>() {
//            @Override
//            public void subscribe(@NonNull ObservableEmitter<Object> emitter) throws Exception {
//                String host = "www.baidu.com";
//                List<InetAddress> dnsList = null;
//                Dns dns = Dns.SYSTEM;
//                if (dns != null) {
//                    try {
//                        dnsList = dns.lookup(host);
//                    } catch (UnknownHostException e) {
//                        e.printStackTrace();
//                    }
//                }
//                JSONObject hostResolverRules = getHostResolverRules(host, dnsList);
//                try {
//                    JSONObject options = new JSONObject().put("HostResolverRules", hostResolverRules);
//                    experimentBuilder.setExperimentalOptions(options.toString());
//                    Log.i(TAG, "HostResolverRules:" + options.toString());
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                emitter.onNext("");
//                emitter.onComplete();
//            }
//        }).subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<Object>() {
//                    @Override
//                    public void accept(Object o) throws Exception {
//
//                    }
//                });

        CronetEngine.Builder engineBuilder = new CronetEngine.Builder(this);
        engineBuilder.enableHttpCache(CronetEngine.Builder.HTTP_CACHE_IN_MEMORY, 100 * 1024)
                .enableHttp2(true)
                .enableQuic(false);
        CronetEngine cronetEngine = engineBuilder.build();
        Executor executor = Executors.newSingleThreadExecutor();
        if (cronetEngine instanceof CronetUrlRequestContext) {
            CronetUrlRequestContext context = (CronetUrlRequestContext) cronetEngine;
            context.addRequestFinishedListener(new RequestFinishedInfo.Listener(executor) {
                @Override
                public void onRequestFinished(RequestFinishedInfo requestInfo) {
                    UrlResponseInfo responseInfo = requestInfo.getResponseInfo();
                    RequestFinishedInfo.Metrics metrics = requestInfo.getMetrics();
                    Log.i(TAG, "metrics:" + metrics);
                    Log.i(TAG, "responseInfo:" + responseInfo);
                }
            });
        }
        Cronet.init(cronetEngine, executor);

        UrlRequest.Callback callback = new UrlRequest.Callback() {
            protected ByteArrayOutputStream mBytesReceived = new ByteArrayOutputStream();
            protected WritableByteChannel mReceiveChannel = Channels.newChannel(mBytesReceived);

            @Override
            public void onRedirectReceived(UrlRequest request, UrlResponseInfo info, String newLocationUrl) {
                request.followRedirect();
            }

            @Override
            public void onResponseStarted(UrlRequest request, UrlResponseInfo info) {
                request.read(ByteBuffer.allocateDirect(32 * 1024));
            }

            @Override
            public void onReadCompleted(UrlRequest request, UrlResponseInfo info, ByteBuffer byteBuffer) {
                byteBuffer.flip();
                try {
                    mReceiveChannel.write(byteBuffer);
                } catch (IOException e) {
                    Log.i(TAG, "IOException during ByteBuffer read. Details: ", e);
                }
                byteBuffer.clear();
                request.read(byteBuffer);
            }

            @Override
            public void onSucceeded(UrlRequest request, UrlResponseInfo info) {
                byte[] bytes = mBytesReceived.toByteArray();
                Log.i(TAG, "success:" + new String(bytes));
            }

            @Override
            public void onFailed(UrlRequest request, UrlResponseInfo info, CronetException error) {
                if (error instanceof NetworkException) {
                    NetworkException exception = (NetworkException) error;
                    Log.i(TAG, "error:" + exception.getErrorCode());
                }
            }
        };

        UrlRequest.Builder builder = cronetEngine.newUrlRequestBuilder("http://op.juhe.cn/onebox/basketball/nba?key=189f1351be5be9d398daab72f3f60ab2", callback, executor);
        String postData = "";
        builder.setHttpMethod("POST");
        builder.addHeader("Content-Type", "application/x-www-form-urlencoded");
        builder.setUploadDataProvider(
                UploadDataProviders.create(postData.getBytes()), executor);
        builder.build().start();
    }

    private void initOkhttp() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
//        builder.addInterceptor(new SpecifiedStringInterceptor());
//        builder.addInterceptor(new CronetInterceptor());
        builder.dns(new MyDns());
        okHttpClient = builder.build();
    }

    private class MyDns implements Dns {

        @NotNull
        @Override
        public List<InetAddress> lookup(@NotNull String s) throws UnknownHostException {
            List<InetAddress> lookup = Dns.SYSTEM.lookup(s);
            Collections.shuffle(lookup);
            Log.i("xyl", "result:" + lookup.get(0));
            return lookup;
        }
    }

    private void initCronet() {

    }

//    private JSONObject getHostResolverRules(String host, List<InetAddress> dnsList) {
//        if (TextUtils.isEmpty(host)) {
//            return null;
//        }
//        StringBuilder hostRuleStr = new StringBuilder();
//        //目前测试看，即使此处设置多个dnsIp， cronet默认也只是用第一个，且第一个失败后不会自动重试第二个
//        for (int i = 0, size = dnsList.size(); i < size; i++) {
//            InetAddress inetAddress = dnsList.get(i);
//            if (inetAddress != null) {
//                String hostAddress = inetAddress.getHostAddress();
//                if (!TextUtils.isEmpty(hostAddress)) {
//                    hostRuleStr.append("MAP ").append(host).append(" ").append(hostAddress).append(",");
//                }
//            }
//        }
//        JSONObject host_rule = null;
//        try {
//            host_rule = new JSONObject().put("host_resolver_rules", hostRuleStr);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return host_rule;
//    }

}
