package com.example.networksample;

import android.text.TextUtils;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * TODO 类描述
 * created by carter on 2021/2/3 14:18
 */
class CloudMusicReportStringInterceptor implements Interceptor {
    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        ResponseBody body = response.body();
        long length = body.contentLength();
        MediaType mediaType = body.contentType();
        String mediaTypeStr = mediaType.type();
        Log.i("xyl", "mediaTypeStr:" + mediaTypeStr);
        // 过大的数据量就忽略掉，防止发生oom
        if (length < 1024) {
            try {
                byte[] bytes = body.bytes();
                String bodyStr = new String(bytes);
                Log.i("xyl", "bodyStr:" + bodyStr);
//                if (bodyStr.contains("")) {
//                    // 上报数据
//                    String requestUrl = request.url().url().toString();
//
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Response newResponse = response.newBuilder().body(response.body()).build();
        return newResponse;
    }

}
